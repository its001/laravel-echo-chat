@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="justify-content-center">
            @foreach(\App\Room::all() as $room)
                <p><a href="{{ route('room.show', $room) }}">{{ $room->name }}</a></p>
            @endforeach
        </div>
    </div>
@endsection
