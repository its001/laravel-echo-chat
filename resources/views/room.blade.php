@extends('layouts.app')

@section('content')
    @auth
    <chat-component
            :users="{{ \App\User::all() }}"
            :user="{{ \Auth::user() }}"
            :room="{{ $room }}"
    ></chat-component>
    @else
        <h3 class="text-center">Пользователь не авторизирован :(</h3>
    @endauth
@endsection
